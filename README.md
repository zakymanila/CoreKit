# CoreKit

Ultimate cross platform framework to create appleOS apps.

### Usage

You can use CoreKit with the following package managers:


#### Carthage

```bash
echo 'git "git@gitlab.com:corekit/CoreKit.git" "master"' >> Cartfile
carthage update
```

#### Swift Package Manager

```swift
dependencies: [
    .package(url: "git@gitlab.com:corekit/CoreKit.git", .branch("master")),
],
```

Enjoy. ;)

#### Notice

If you prefer HTTP over SSH please replace:

```
git@gitlab.com:corekit/CoreKit.git -> https://gitlab.com/corekit/CoreKit
```

### License

[WTFPL](LICENSE)
