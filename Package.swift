// swift-tools-version:4.2

import PackageDescription

// This is going to speed up builds on pure Linux & Darwin
// Basically this will only use Swift, Foundation and Dispatch modules
let excludePaths = [
                    "AppleKit",
                    "ARKit",
                    "AVFoundation",
                    "CoreGraphics",
                    "CoreKit",
                    "CoreLocation",
                    "QuartzCore",
                    "SpriteKit",
                    "StoreKit",
                    "WatchKit"
                ]

let package = Package(
    name: "CoreKit",
    products: [
        .library(
            name: "CoreKit",
            targets: ["CoreKit"]
        )
    ],
    dependencies: [
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        .target(
            name: "CoreKit",
            dependencies: [],
            path: "./Sources/CoreKit",
            exclude: excludePaths
            //sources: ["main.swift"]
        ),
        .testTarget(
            name: "CoreKitTests",
            dependencies: ["CoreKit"],
            path: "./Tests/Sources",
            exclude: excludePaths
        )
    ]
)
