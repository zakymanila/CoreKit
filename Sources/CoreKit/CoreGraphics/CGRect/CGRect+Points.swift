//
//  CGRect+Points.swift
//  CoreKit
//
//  Created by Tibor Bödecs on 2018. 05. 23..
//  Copyright © 2018. Tibor Bödecs. All rights reserved.
//

#if canImport(CoreGraphics)

public extension CGRect {

    public var minXminY: CGPoint { return CGPoint(x: self.minX, y: self.minY) }
    public var minXmidY: CGPoint { return CGPoint(x: self.minX, y: self.midY) }
    public var minXmaxY: CGPoint { return CGPoint(x: self.minX, y: self.maxY) }
    public var midXminY: CGPoint { return CGPoint(x: self.midX, y: self.minY) }
    public var midXmidY: CGPoint { return CGPoint(x: self.midX, y: self.midY) }
    public var midXmaxY: CGPoint { return CGPoint(x: self.midX, y: self.maxY) }
    public var maxXminY: CGPoint { return CGPoint(x: self.maxX, y: self.minY) }
    public var maxXmidY: CGPoint { return CGPoint(x: self.maxX, y: self.midY) }
    public var maxXmaxY: CGPoint { return CGPoint(x: self.maxX, y: self.maxY) }
    public var center: CGPoint { return self.midXmidY }

}

#endif
