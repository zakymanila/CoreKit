//
//  AppleCollectionViewReusableView.swift
//  CoreKit
//
//  Created by Tibor Bödecs on 2017. 09. 29..
//  Copyright © 2017. Tibor Bödecs. All rights reserved.
//

#if os(iOS) || os(tvOS)

    public typealias AppleCollectionViewReusableView = UICollectionReusableView

    public let AppleCollectionElementKindSectionHeader = UICollectionView.elementKindSectionHeader
    public let AppleCollectionElementKindSectionFooter = UICollectionView.elementKindSectionFooter

#elseif os(macOS)

    public typealias AppleCollectionViewReusableView = NSCollectionViewItem

    public let AppleCollectionElementKindSectionHeader = NSCollectionView.elementKindSectionHeader
    public let AppleCollectionElementKindSectionFooter = NSCollectionView.elementKindSectionFooter

#endif
