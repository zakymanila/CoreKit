//
//  AppleCollectionViewCell+Kind.swift
//  CoreKit
//
//  Created by Tibor Bödecs on 2017. 10. 12..
//  Copyright © 2017. Tibor Bödecs. All rights reserved.
//

#if os(macOS) || os(iOS) || os(tvOS)

    /**
     Kind
     */
    public extension AppleCollectionViewCell {

        public enum Kind: RawRepresentable {
            public typealias RawValue = String

            case header
            case footer
            case custom(String)

            public init?(rawValue: RawValue) {
                switch rawValue {
                case AppleCollectionElementKindSectionHeader:
                    self = .header
                case AppleCollectionElementKindSectionFooter:
                    self = .footer
                default:
                    self = .custom(rawValue)
                }
            }

            public var rawValue: RawValue {
                switch self {
                case .header:
                    return AppleCollectionElementKindSectionHeader
                case .footer:
                    return AppleCollectionElementKindSectionFooter
                case .custom(let string):
                    return string
                }
            }
        }
    }
#endif
